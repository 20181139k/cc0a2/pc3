package pe.uni.davisalderetev.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Basica extends AppCompatActivity {
    Button button0,button1,button2,button3,button4,button5,button6,button7,button8,buttonSum,buttonRes,buttonMul,buttonClean,buttonResult;
    TextView textViewResult;

    int num_0,num_1,num_2,num_3,num_4,num_5,num_6,num_7,num_8;
    boolean sum,res,mul;
    String restext="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basica);
        button0=findViewById(R.id.button_cal_0);
        button1=findViewById(R.id.button_cal_1);
        button2=findViewById(R.id.button_cal_2);
        button3=findViewById(R.id.button_cal_3);
        button4=findViewById(R.id.button_cal_4);
        button5=findViewById(R.id.button_cal_5);
        button6=findViewById(R.id.button_cal_6);
        button7=findViewById(R.id.button_cal_7);
        button8=findViewById(R.id.button_cal_8);

        buttonSum=findViewById(R.id.button_op_sum);
        buttonRes=findViewById(R.id.button_op_res);
        buttonMul=findViewById(R.id.button_op_mult);

        buttonResult=findViewById(R.id.button_result);
        buttonClean=findViewById(R.id.button_clean);


        textViewResult=findViewById(R.id.text_view_result);

        buttonClean.setOnClickListener(v -> textViewResult.setText(""));


        button0.setOnClickListener(v -> {
            restext=restext+String.valueOf(0);
            textViewResult.setText(restext);
        });
        button1.setOnClickListener(v -> {
            restext=restext+String.valueOf(1);
            textViewResult.setText(restext);});

        button2.setOnClickListener(v -> {
            restext=restext+String.valueOf(2);
            textViewResult.setText(restext);});
        button3.setOnClickListener(v -> {
            restext=restext+String.valueOf(3);
            textViewResult.setText(restext);});
        button4.setOnClickListener(v -> {
            restext=restext+String.valueOf(4);
            textViewResult.setText(restext);});
        button5.setOnClickListener(v -> {
            restext=restext+String.valueOf(5);
            textViewResult.setText(restext);});
        button6.setOnClickListener(v -> {
            restext=restext+String.valueOf(6);
            textViewResult.setText(restext);});
        button7.setOnClickListener(v -> {
            restext=restext+String.valueOf(7);
            textViewResult.setText(restext);});
        button8.setOnClickListener(v -> {
            restext=restext+String.valueOf(8);
            textViewResult.setText(restext);});

        buttonSum.setOnClickListener(v -> sum=true);
        buttonRes.setOnClickListener(v -> res=true);
        buttonMul.setOnClickListener(v -> mul=true);


    }
}