package pe.uni.davisalderetev.pc3;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

import com.google.android.material.snackbar.Snackbar;

public class Calculadora extends AppCompatActivity {
    RadioButton radioButtonOne,radioButtonTwo,radioButtonThree;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
    radioButtonOne=findViewById(R.id.radio_button_basic);
    radioButtonTwo=findViewById(R.id.radio_button_cienti);
    radioButtonThree=findViewById(R.id.radio_button_programmer);

    button=findViewById(R.id.button_choose_cal);

        button.setOnClickListener(v -> {
            if (!radioButtonOne.isChecked() && !radioButtonTwo.isChecked() && !radioButtonThree.isChecked()){
                Snackbar.make(v,R.string.snack_bar_message,Snackbar.LENGTH_LONG).show();
                return;
            }
            Intent intent=new Intent(Calculadora.this,Basica.class);
            if(radioButtonOne.isChecked()){

                intent.putExtra("BASIC",true);

                startActivity(intent);

            }
            if(radioButtonTwo.isChecked()){
                //intent.putExtra("CIENTIFIC",true);
                AlertDialog.Builder builder=new AlertDialog.Builder(Calculadora.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.message_build);
                builder.setPositiveButton("Si", (dialog, which) -> {

                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    //liberar el proceso
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }
            if(radioButtonThree.isChecked()){
                //intent.putExtra("PROGRAMMER",true);
                AlertDialog.Builder builder=new AlertDialog.Builder(Calculadora.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(R.string.message_build);
                builder.setPositiveButton("Si", (dialog, which) -> {

                });
                builder.setNegativeButton("NO", (dialog, which) -> {
                    moveTaskToBack(true);
                    //liberar el proceso
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                });
                builder.create().show();
            }

        });


    }
}